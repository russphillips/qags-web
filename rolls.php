<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
require("inc_head_html.php");
// Initialise $log
$log = "";
// Initialise $rerolloption
$rerolloption = False;
// Re-roll button text is important, so use a constant to ensure consistency
define ("RE_ROLL_TEXT", "Re-Roll");

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	if (isset ($_POST["note"]) and $_POST["note"] != "")
		$note = "<i>Note: " . htmlentities($_POST["note"], ENT_QUOTES) . "</i><br>";
	else
		$note = "";
	
	// Character 1 details
	$htmlchar1 = htmlentities($db->querySingle("SELECT name FROM characters WHERE charid = ".intval($_POST["character1"])),ENT_QUOTES);
	if ($_POST["word1"] == "body" || $_POST["word1"] == "brain" || $_POST["word1"] == "nerve") {
		$word1name = htmlentities(ucwords($_POST["word1"]), ENT_QUOTES);
		$word1value = $db->querySingle("SELECT ".$_POST["word1"]." FROM characters WHERE charid = ".intval($_POST["character1"]));
		$word1type = $_POST["word1"];
	}
	else {
		$word = $db->querySingle("SELECT * FROM words WHERE wordid = ".intval($_POST["word1"]), True);
		$word1name = htmlentities($word["word"], ENT_QUOTES);
		$word1value = $word ["value"];
		$word1type = strtolower($word ["type"]);
	}
	
	if ($_POST["btnSubmit"] == RE_ROLL_TEXT) {
		$log .= "$htmlchar1 is spending three yum yums to re-roll<br>";
		$sql = "UPDATE characters SET yumyums = yumyums-3 WHERE charid = ".intval($_POST["character1"]);
		$db->exec($sql);
	}
	
	// Simple roll
	if ($_POST["rdoType"] == "simple") {
		// Make the roll
		$dieroll = mt_rand (1, 20);

		// Log results
		$log .= "$htmlchar1 making a simple roll, using $word1name ($word1value)<br>$note";

		// Default roll
		if (isset ($_POST["default1"])) {
			$word1value = floor ($word1value/2);
			$log .= "This is a default roll, so Number is $word1value<br>";
		}
		// Skills
		foreach ($_POST as $key=>$value) {
			if (substr ($key, 0, 11) == "chkskills1_") {
				$skillid = substr ($key, 11);
				$skillname = htmlentities($db->querySingle("SELECT word FROM words WHERE wordid = $skillid"), ENT_QUOTES);
				$log .= "Using skill $skillname (+$value)<br>";
				$word1value += $value;
			}
		}
		// Other modifier
		if (isset($_POST["modifier1"]) && intval($_POST["modifier1"]) != 0) {
			$log .= "Modifier: ".intval($_POST["modifier1"])."<br>";
			$word1value += intval($_POST["modifier1"]);
		}

		// Roll die
		$log .= "Modified Number: $word1value<br>";
		$log .= "Die roll: $dieroll<br>";
		
		// Random acts of fate
		if ($dieroll == 1 && $word1value >= $dn) {
			$log .= "<p class='good'>Quirky success! (".abs($level).": See Random Acts of Fate, QAGS page 21)";
			if ($word1type == "weakness")
				$weaknessresult = "succumbed to";
		}
		elseif ($dieroll == $word1value) {
			$log .= "<p class='good'>Success! (".abs($level).": Lucky Break - see Random Acts of Fate, QAGS page 21)";
			if ($word1type == "weakness")
				$weaknessresult = "succumbed to";
		}
		elseif ($dieroll == 20) {
			$log .= "<p class='bad'>Failed! (".abs($level).": Bad Break - see Random Acts of Fate, QAGS page 21)";
			if ($word1type == "weakness")
				$weaknessresult = "resisted";
			// Allow a re-roll
			$rerolloption = True;
		}
		else {
			// Standard success/failure
			if ($dieroll <= $word1value) {
				$log .= "<p class='";
				if ($word1type == "weakness") {
					$log .= "bad";
					$weaknessresult = "succumbed to";
				}
				else {
					$log .= "good";
					$weaknessresult = "resisted";
				}
				$log .= "'>Success! ";
				$level = $dieroll;
			}
			else {
				$log .= "<p class='";
				if ($word1type == "weakness") {
					$log .= "good";
					$weaknessresult = "resisted";
				}
				else {
					$log .= "bad";
					$weaknessresult = "succumbed to";
				}
				$log .= "'>Failed! ";
				$level = $dieroll - $word1value;
				// Allow a re-roll
				$rerolloption = True;
			}
			// Work out level of success/failure
			if (abs($level) <= 5)
				$log .= "(".abs($level).": Minimal";
			elseif (abs($level) <= 10)
				$log .= "(".abs($level).": Average";
			elseif (abs($level) <= 15)
				$log .= "(".abs($level).": Impressive";
			else
				$log .= "(".abs($level).": Spectacular";
			$log .= " - see Dumb Table 2, QAGS page 21)";
			if ($word1type == "weakness")
				$log .= "<br>$htmlchar1 $weaknessresult their weakness";
			$log .= "</p>";
		}
	}

	// Difficulty number
	if ($_POST["rdoType"] == "dn") {
		$dn = intval($_POST["dn"]);
		// Make the roll
		$dieroll = mt_rand (1, 20);

		// Log results
		$log .= "$htmlchar1 making a roll against difficulty number $dn, using $word1name ($word1value)<br>$note";

		// Default roll
		if (isset ($_POST["default1"])) {
			$word1value = floor ($word1value/2);
			$log .= "This is a default roll, so Number is $word1value<br>";
		}
		// Skills
		foreach ($_POST as $key=>$value) {
			if (substr ($key, 0, 11) == "chkskills1_") {
				$skillid = substr ($key, 11);
				$skillname = htmlentities($db->querySingle("SELECT word FROM words WHERE wordid = $skillid"), ENT_QUOTES);
				$log .= "Using skill $skillname (+$value)<br>";
				$word1value += $value;
			}
		}
		// Other modifier
		if (isset($_POST["modifier1"]) && intval($_POST["modifier1"]) != 0) {
			$log .= "Modifier: ".intval($_POST["modifier1"])."<br>";
			$word1value += intval($_POST["modifier1"]);
		}

		// Roll die
		$log .= "Modified Number: $word1value<br>";
		$log .= "Die roll: $dieroll<br>";
		
		// Random acts of fate
		if ($dieroll == 1 && $word1value >= $dn) {
			$log .= "<p class='good'>Quirky success! (".abs($level).": See Random Acts of Fate, QAGS page 21)</p>\n";
			if ($word1type == "weakness")
				$weaknessresult = "succumbed to";
		}
		elseif ($dieroll == $word1value) {
			$log .= "<p class='good'>Success! (".abs($level).": Lucky Break - see Random Acts of Fate, QAGS page 21)</p>\n";
			if ($word1type == "weakness")
				$weaknessresult = "succumbed to";
		}
		elseif ($dieroll == 20) {
			$log .= "<p class='bad'>Failed! (".abs($level).": Bad Break - see Random Acts of Fate, QAGS page 21)";
			if ($word1type == "weakness")
				$weaknessresult = "resisted";
			// Allow a re-roll
			$rerolloption = True;
		}
		else {
			// Standard success/failure
			if ($dieroll >= $dn && $dieroll <= $word1value) {
				$log .= "<p class='";
				if ($word1type == "weakness") {
					$log .= "bad";
					$weaknessresult = "succumbed to";
				}
				else {
					$log .= "good";
					$weaknessresult = "resisted";
				}
				$log .= "'>Success! ";
				$level = $dieroll;
			}
			else {
				$log .= "<p class='";
				if ($word1type == "weakness") {
					$log .= "good";
					$weaknessresult = "resisted";
				}
				else {
					$log .= "bad";
					$weaknessresult = "succumbed to";
				}
				$log .= "'>Failed! ";
				$level = $dieroll - $word1value;
				// Allow a re-roll
				$rerolloption = True;
			}
			// Work out level of success/failure
			if (abs($level) <= 5)
				$log .= "(".abs($level).": Minimal";
			elseif (abs($level) <= 10)
				$log .= "(".abs($level).": Average";
			elseif (abs($level) <= 15)
				$log .= "(".abs($level).": Impressive";
			else
				$log .= "(".abs($level).": Spectacular";
			$log .= " - see Dumb Table 2, QAGS page 21)";
			if ($word1type == "weakness")
				$log .= "<br>$htmlchar1 $weaknessresult their weakness";
			$log .= "</p>";
		}
	}
	
	// Resisted roll
	if ($_POST["rdoType"] == "resist") {
		// Character 2 details
		$htmlchar2 = htmlentities($db->querySingle("SELECT name FROM characters WHERE charid = ".intval($_POST["character2"])),ENT_QUOTES);
		if ($_POST["word2"] == "body" || $_POST["word2"] == "brain" || $_POST["word2"] == "nerve") {
			$word2name = htmlentities(ucwords($_POST["word2"]), ENT_QUOTES);
			$word2value = $db->querySingle("SELECT ".$_POST["word2"]." FROM characters WHERE charid = ".intval($_POST["character2"]));
		}
		else {
			$word = $db->querySingle("SELECT * FROM words WHERE wordid = ".intval($_POST["word2"]), True);
			$word2name = htmlentities($word["word"], ENT_QUOTES);
			$word2value = $word ["value"];
		}

		// Log results
		$log .= "$htmlchar1 ($word1name $word1value) making a resisted roll against $htmlchar2 ($word2name $word2value)<br>$note";

		// Default roll (character 1)
		if (isset ($_POST["default1"])) {
			$word1value = floor ($word1value/2);
			$log .= "$htmlchar1 is making a default roll, so Number is $word1value<br>";
		}
		// Skills (character 1)
		foreach ($_POST as $key=>$value) {
			if (substr ($key, 0, 11) == "chkskills1_") {
				$skillid = substr ($key, 11);
				$skillname = htmlentities($db->querySingle("SELECT word FROM words WHERE wordid = $skillid"), ENT_QUOTES);
				$log .= "$htmlchar1 is using skill $skillname (+$value)<br>";
				$word1value += $value;
			}
		}
		// Other modifier (character 1)
		if (isset($_POST["modifier1"]) && intval($_POST["modifier1"]) != 0) {
			$log .= "$htmlchar1 modifier: ".intval($_POST["modifier1"])."<br>";
			$word1value += intval($_POST["modifier1"]);
		}
		
		// Default roll (character 2)
		if (isset ($_POST["default2"])) {
			$word2value = floor ($word2value/2);
			$log .= "$htmlchar2 is making a default roll, so Number is $word2value<br>";
		}
		// Skills (character 2)
		foreach ($_POST as $key=>$value) {
			if (substr ($key, 0, 11) == "chkskills2_") {
				$skillid = substr ($key, 11);
				$skillname = htmlentities($db->querySingle("SELECT word FROM words WHERE wordid = $skillid"), ENT_QUOTES);
				$log .= "$htmlchar2 is using skill $skillname (+$value)<br>";
				$word2value += $value;
			}
		}
		// Other modifier (character 2)
		if (isset($_POST["modifier2"]) && intval($_POST["modifier2"]) != 0) {
			$log .= "$htmlchar2 modifier: ".intval($_POST["modifier2"])."<br>";
			$word2value += intval($_POST["modifier2"]);
		}

		// Roll dice
		$dieroll1 = mt_rand (1, 20);
		$dieroll2 = mt_rand (1, 20);
		
		$log .= "$htmlchar1 rolls $dieroll1 (";
		if ($dieroll1 <= $word1value)
			$log .= "Success";
		else
			$log .= "Failed";
		$log .= ")<br>";
		
		$log .= "$htmlchar2 rolls $dieroll2 (";
		if ($dieroll2 <= $word2value)
			$log .= "Success";
		else
			$log .= "Failed";
		$log .= ")<br>";
		
		// Results
		if ($dieroll1 > $word1value && $dieroll2 > $word2value)
			$log .= "<p class='bad'>Both characters failed their roll.</p>\n";
		elseif ($dieroll1 <= $word1value && $dieroll2 > $word2value)
			$log .= "<p class='good'>$htmlchar1 wins!</p>\n";
		elseif ($dieroll1 > $word1value && $dieroll2 <= $word2value)
			$log .= "<p class='good'>$htmlchar2 wins!</p>\n";
		elseif ($dieroll1 <= $word1value && $dieroll2 <= $word2value) {
			if ($dieroll1 > $dieroll2)
				$log .= "<p class='good'>$htmlchar1 wins!</p>\n";
			elseif ($dieroll1 < $dieroll2)
				$log .= "<p class='good'>$htmlchar2 wins!</p>\n";
			elseif ($dieroll1 == $dieroll2)
				$log .= "<p class='bad'>Both rolls succeeded. Result is a tie. GM may declare a stalemate or have the players reroll. See QAGS page 20</p>\n";
		}
	}
	
	// Log the result
	logdb ($log);
}

// Defaults for AJAX
if (isset($_POST["word1"]))
	$word1default = urlencode($_POST["word1"]);
else
	$word1default = "";
if (isset($_POST["word2"]))
	$word2default = urlencode($_POST["word2"]);
else
	$word2default = "";
?>

<script>
$(function() {
	// Get word lists at load
	$("#word1").load("./ajax_wordoptionlist.php?charid="+$("#character1").val()+"&default=<?=$word1default;?>")
	$("#skills1").load("./ajax_skillcheckboxlist.php?charid="+$("#character1").val()+"&basename=chkskills1_")

	$('#word2').load('./ajax_wordoptionlist.php?charid='+$("#character2").val()+'&default=<?=$word2default;?>')
	$('#skills2').load('./ajax_skillcheckboxlist.php?charid='+$("#character2").val()+'&basename=chkskills2_')

	// Update word lists when character changes
	$("#character1").change(function(event) {
		$("#word1").load("./ajax_wordoptionlist.php?charid="+$("#character1").val()+"&default=<?=$word1default;?>")
		$("#skills1").load("./ajax_skillcheckboxlist.php?charid="+$("#character1").val()+"&basename=chkskills1_")
	});
	$("#character2").change(function(event) {
		$("#word2").load("./ajax_wordoptionlist.php?charid="+$("#character2").val()+"&default=<?=$word2default;?>")
		$("#skills2").load("./ajax_skillcheckboxlist.php?charid="+$("#character2").val()+"&basename=chkskills2_")
	});
	
	// Hide/show DN box
	$("input[type='radio']").change(function(event) {
		if ($(this).val() == "simple") {
			$("#divdn").hide()
			$("#divresist").hide()
			$("#char1title").text("Character")
		}
		if ($(this).val() == "dn") {
			$("#divdn").show()
			$("#divresist").hide()
			$("#char1title").text("Character")
		}
		if ($(this).val() == "resist") {
			$("#divdn").hide()
			$("#divresist").show()
			$("#char1title").text("Character 1")
			$("#char2title").text("Character 2")
		}
	})
	
	// DN value changed - update description
	$("#dn").keyup(function(event) {
		dnChange()
	})
	$("#dn").change(function(event) {
		dnChange()
	})
	
	function dnChange() {
		if ($("#dn").val() >= 19)
			$("#dndescription").text("(Just plain nuts)")
		else if ($("#dn").val() >= 15)
			$("#dndescription").text("(Herculean)")
		else if ($("#dn").val() >= 10)
			$("#dndescription").text("(Tough)")
		else if ($("#dn").val() >= 5)
			$("#dndescription").text("(Tricky)")
		else
			$("#dndescription").text("(Easy)")
	}
	
	// Hide results box on rolling dice
	$(".btnSubmit").click(function(event) {
		$("#results").hide()
	})
})
</script>

<h1>Make a Roll</h1>

<form method="post">
<!-- word type for word 1 - only matters if it is a weakness -->
<div class="box">
<p class="boxtitle">Type of Roll</p>
<p>
<?php
// Set up variables to check relevant radio button and show relevant div
$checkedsimple = "";
$checkeddn = "";
$styledn = " style='display:none;'";
$checkedresist = "";
$styleresist = " style='display:none;'";

if (isset($_POST["rdoType"]) && $_POST["rdoType"] == "dn") {
	$checkeddn = " checked";
	$styledn = "style='display:inline-block;'";
}
elseif (isset($_POST["rdoType"]) && $_POST["rdoType"] == "resist") {
	$checkedresist = " checked";
	$styleresist = "style='display:inline-block;'";
}
else
	$checkedsimple = " checked";
?>
<input type="radio" id="rdosimple" name="rdoType" value="simple"<?=$checkedsimple;?>> <label for="rdosimple">Simple roll</label><br>
<input type="radio" id="rdodn" name="rdoType" value="dn"<?=$checkeddn;?>> <label for="rdodn">Roll against a difficulty number</label><br>
<input type="radio" id="rdoresist" name="rdoType" value="resist"<?=$checkedresist;?>> <label for="rdoresist">Resisted roll</label>
</p>
</div>

<div class="box">
<p class="boxtitle" id="char1title">Character</p>
<p>
<select name="character1" id="character1">
<?php
if (isset ($_POST["character1"]))
	$char1id = intval($_POST["character1"]);
else
	$char1id = CHARACTERID;
selectCharacters($db, $char1id);
?>
</select><br>
Word: <select name="word1" id="word1">
</select>
</p>
<p class="boxtitle">Skills</p>
<p id="skills1">
</p>
<p class="boxtitle">Other</p>
<?php
if (isset($_POST["default1"]))
	$checked=" checked";
else
	$checked="";
if (isset($_POST["modifier1"]))
	$othermod = intval($_POST["modifier1"]);
else
	$othermod = "";
if (isset($_POST["dn"]))
	$dn = intval($_POST["dn"]);
else
	$dn = "";
?>
<p>
<input type="checkbox" id="default1" name="default1"<?=$checked;?>> <label for="default1">This is a default roll</label><br>
Other modifier: <input class="small" name="modifier1" value="<?=$othermod;?>" type="number">
</p>
</div>

<!-- DN div -->
<div class="box" <?=$styledn;?> id="divdn">
<p class="boxtitle">Difficulty Number</p>
<p>
<input name="dn" class="small" id="dn" value="<?=$dn;?>" type="number">&nbsp;
<span id="dndescription"></span>
</p>
</div>

<!-- Opposed div -->
<div class="box" <?=$styleresist;?> id="divresist">
<p class="boxtitle" id="char2title">Character</p>
<p>
<select name="character2" id="character2">
<?php
selectCharacters($db, intval($_POST["character2"]));
?>
</select><br>
Word: <select name="word2" id="word2">
</select>
</p>
<p class="boxtitle">Skills</p>
<p id="skills2">
</p>
<p class="boxtitle">Other</p>
<p>
<p>
<?php
if (isset($_POST["default2"]))
	$checked=" checked";
else
	$checked="";
if (isset($_POST["modifier2"]))
	$othermod = intval($_POST["modifier2"]);
else
	$othermod = "";
?>
<input type="checkbox" id="default2" name="default2"<?=$checked;?>> <label for="default2">This is a default roll</label><br>
Other modifier: <input class="small" name="modifier2" value="<?=$othermod;?>" type="number">
</p>
</div>

<p>
Note: <input name="note" class="mid" value="<?=htmlentities($_POST["note"], ENT_QUOTES);?>">
</p>
<p>
<input type="submit" name="btnSubmit" class="btnSubmit" value="Roll the dice">
</p>

<?php
if ($log != "") {
	// Show results
	echo "<div class='box' id='results'><h2>Results</h2>\n";
	echo "<p>$log</p>\n";
	
	// Show re-roll option
	if ($_POST["btnSubmit"] != RE_ROLL_TEXT && $rerolloption === True) {
		echo "<hr>";
		$yy = $db->querySingle("SELECT yumyums FROM characters WHERE charid = ".intval($_POST["character1"]));
		if ($yy < 3)
			echo "<p>You only have {$yy}YY. It costs three to re-roll a failed die roll.</p>\n";
		else
			echo "<p>Spend three of your $yy yum yums to re-roll: <input type='submit' class='btnSubmit' name='btnSubmit' value='".RE_ROLL_TEXT."'></p>\n";
	}
	
	echo "</div>\n";
}
echo "</form>\n";

require("inc_foot.php");
?>
