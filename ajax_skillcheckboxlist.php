<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require_once("inc_head_php.php");

// Load a list of words into a <SELECT>
$ajaxdb = new SQLite3(DBFILE);
$charid = intval($_GET["charid"]);
$basename = htmlentities($_GET["basename"], ENT_QUOTES);

$sql = "SELECT * FROM words WHERE charid = $charid AND type LIKE 'skill' ORDER BY value DESC";
$words = $ajaxdb->query($sql);
while ($word = $words->fetchArray(SQLITE3_ASSOC))
	echo "<input type='checkbox' value='".$word["value"]."' name='$basename".$word["wordid"]."' id='$basename".$word["wordid"]."'> <label for='$basename".$word["wordid"]."'>".htmlentities($word["word"], ENT_QUOTES)." (+".$word["value"].")</label><br>";

$ajaxdb->close();
?>
