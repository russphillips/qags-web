<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
require("inc_head_html.php");
?>
<script>
$(function() {
	// Set initial text for Active label
	if ($("#gmc").prop("checked"))
		$("#lblActive").text("Active GMC")
	else
		$("#lblActive").text("Active PC")

	// Update Active label text if GMC checkbox changes
	$("#gmc").change(function () {
		if ($("#gmc").prop("checked"))
			$("#lblActive").text("Active GMC")
		else
			$("#lblActive").text("Active PC")
	})
})
</script>
<?php
if (isset($_GET["id"])) {
	$charid = intval($_GET["id"]);

	if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
		// Update players table to set character's player
		$sql = "UPDATE players SET player_charid = 0 WHERE player_charid = $charid";
		$db->exec($sql);
		$sql = "UPDATE players SET player_charid = $charid WHERE playerid = ".intval($_POST["player"]);
		$db->exec($sql);
		
		// Update characters table
		if (isset($_POST["gmc"]))
			$gmc = 1;
		else
			$gmc = 0;
		if (isset($_POST["active"]))
			$active = 1;
		else
			$active = 0;
		$sql = "UPDATE characters SET ".
			"name='".$db->escapeString($_POST["name"])."', ".
			"body=".intval($_POST["body"]).", ".
			"brain=".intval($_POST["brain"]).", ".
			"nerve=".intval($_POST["nerve"]).", ".
			"hp=".intval($_POST["hp"]).", ".
			"currenthp=".intval($_POST["currenthp"]).", ".
			"yumyums=".intval($_POST["yumyums"]).", ".
			"concept='".$db->escapeString($_POST["concept"])."', ".
			"tagline='".$db->escapeString($_POST["tagline"])."', ".
			"dumbfact='".$db->escapeString($_POST["dumbfact"])."', ".
			"notes='".$db->escapeString($_POST["notes"])."', ".
			"gmnotes='".$db->escapeString($_POST["gmnotes"])."', ".
			"wwphitm='".$db->escapeString($_POST["wwphitm"])."', ".
			"gmc=$gmc, ".
			"active=$active ".
			"WHERE charid = $charid";
		$db->exec($sql);

		// Update existing words
		foreach ($_POST as $key=>$value) {
			if (substr($key, 0, 6) == "wordid" && strlen($key) > 6) {
				$id = $value;
				$sql = "UPDATE words SET word = '".$db->escapeString($_POST["word$id"])."',
					value = ".intval($_POST["wordvalue$id"])."
					WHERE wordid = $id";
				$db->exec($sql);
			}
		}

		// Delete words
		foreach ($_POST as $key=>$value) {
			$sql = "";
			if (substr($key, 0, 10) == "worddelete" && strlen($key) > 10)
				$sql = "DELETE FROM words WHERE wordid = ".substr($key, 10)." LIMIT 1";
			$db->exec($sql);
		}

		// Add new word
		if (isset($_POST["newword"]) && $_POST["newword"] != "") {
			$sql = "INSERT INTO words (type, charid, word, value) VALUES ('".$db->escapeString($_POST["newtype"])."', $charid, '".$db->escapeString($_POST["newword"])."', ".intval($_POST["newvalue"]).")";
			$db->exec($sql);
		}
		
		// Group memberships - remove from all groups
		$sql = "DELETE FROM groupmembers WHERE gm_charid = $charid";
		$db->exec($sql);
		// Now add to the selected groups
		foreach ($_POST as $key=>$value) {
			$sql = "";
			if (substr($key, 0, 5) == "group" && strlen($key) > 5)
				$sql = "INSERT INTO groupmembers (gm_groupid, gm_charid) VALUES (".substr($key, 5).", $charid)";
			$db->exec($sql);
		}
	}

	$sql = "SELECT characters.*, playerid FROM characters LEFT JOIN players ON player_charid = charid WHERE charid = $charid";
	$char = $db->querySingle($sql, True);

	echo "<h1>Edit Character: ".htmlentities($char["name"], ENT_QUOTES)."</h1>\n";
	if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "")
		echo "<p class='good'>Character updated.</p>";
	?>

	<form method="post">
	<p>
	Player: <select name="player">
	<?php
	echo "<option value='0'";
	if (intval($char["playerid"]) == 0)
		echo " selected";
	echo ">None</option>";
	
	$sql = "SELECT playerid, name FROM players ORDER BY name";
	$players = $db->query($sql);
	while ($player = $players->fetchArray(SQLITE3_ASSOC)) {
		echo "<option value='".$player["playerid"]."'";
		if (intval($char["playerid"]) == $player["playerid"])
			echo " selected";
		echo ">".htmlentities($player["name"], ENT_QUOTES)."</option>";
	}
	?>
	</select>
	</p>
	<div class="box">
	<p>
	Name: <input name="name" required value="<?=htmlentities($char["name"], ENT_QUOTES);?>"><br>
	Concept: <input name="concept" value="<?=htmlentities($char["concept"], ENT_QUOTES);?>"><br>
	Tag Line: <input name="tagline" value="<?=htmlentities($char["tagline"], ENT_QUOTES);?>"><br>
	<div class='innerthird'>
	Body: <input name="body" class="small" required type="number" value="<?=$char["body"];?>"><br>
	Brain: <input name="brain" class="small" required type="number" value="<?=$char["brain"];?>"><br>
	<?php
	if ($char["gmc"] == 1)
		$check = " checked";
	else
		$check = "";
	?>
	<input type="checkbox" id="gmc" name="gmc" title="Tick this box if the character is a GMC"<?=$check;?>> <label for="gmc" title="Tick this box if the character is a GMC">This is a GMC</label>
	</p>
	</div>
	<div class='innerthird'>
	Nerve: <input name="nerve" class="small" required type="number" value="<?=$char["nerve"];?>"><br>
	Yum Yums: <input name="yumyums" class="small" required type="number" value="<?=$char["yumyums"];?>"><br>
	<?php
	if ($char["active"] == 1)
		$check = " checked";
	else
		$check = "";
	?>
	<input type="checkbox" id="active" name="active" title="Tick this box if the character is active"<?=$check;?>> <label for="active" title="Tick this box if the character is active" id="lblActive">Active</label>
	</div>
	<div class='innerthird'>
	Health: <input name="hp" class="small" required type="number" value="<?=$char["hp"];?>"><br>
	Current Health: <input name="currenthp" class="small" required type="number" value="<?=$char["currenthp"];?>"><br>
	</div>
	</div>

	<h2>Words</h2>
	<div class="box">
		<div id="wordsdiv">
		<?php
		$sql = "SELECT wordid, type, word, value FROM words WHERE charid = $charid ORDER BY type, value DESC";
		$words = $db->query($sql);
		while ($word = $words->fetchArray(SQLITE3_ASSOC)) {
			$id = $word["wordid"];
			echo "<div class='word'><input type='hidden' name='wordid$id' value='$id'>".htmlentities(ucwords($word["type"]), ENT_QUOTES);
			echo ": <input name='word$id' value='".htmlentities($word["word"], ENT_QUOTES)."' class='mid'>";
			echo "<input name='wordvalue$id' value='".intval($word["value"])."' class='small' type='number'>";
			echo "<input type='checkbox' name='worddelete$id'> Delete</div>";
		}
		?>
			<div class='word'><select name="newtype" title="Select type">
			<option value="Job">Job</option>
			<option value="Gimmick">Gimmick</option>
			<option value="Weakness">Weakness</option>
			<option value="Skill">Skill</option>
			</optgroup>
			</select>
			<input name="newword" class="mid" placeholder="Enter new word here">
			<input name="newvalue" class="small" title="Value of new word" type="number">
			</div>
		</div>
	</div>

	<h2>Notes etc.</h2>
	<div class="box">
	<p>
	Dumb Fact: <input name="dumbfact" value="<?=htmlentities($char["dumbfact"], ENT_QUOTES);?>"><br>
	WWPHITM: <input name="wwphitm" value="<?=htmlentities($char["wwphitm"], ENT_QUOTES);?>"><br>
	Notes:<br>
	<textarea name="notes"><?=htmlentities($char["notes"], ENT_QUOTES);?></textarea><br>
	GM Notes:<br>
	<textarea name="gmnotes"><?=htmlentities($char["gmnotes"], ENT_QUOTES);?></textarea>
	</p>
	</div>

	<h2>Groups</h2>
	<div class="box">
		<?php
		$sql = "SELECT * FROM groups ORDER by groupname";
		$groups = $db->query($sql);
		while ($group = $groups->fetchArray(SQLITE3_ASSOC)) {
			// Tick the box if the character is in the group
			$sql = "SELECT COUNT(*) FROM groupmembers WHERE gm_groupid=".$group["groupid"]." AND gm_charid=$charid";
			if ($db->querySingle($sql) == 1)
				$check = " checked";
			else
				$check = "";
			echo "<input type='checkbox'$check name='group".$group["groupid"]."' id='g".$group["groupid"]."'><label for='g".$group["groupid"]."'> ".htmlentities($group["groupname"], ENT_QUOTES)."</label>&nbsp;";
		}
		?>
	</div>

	<p>
	<input type="submit" name="btnSubmit" value="Save">
	</p>
	</form>
	
	<hr>
<?php
}
if (!isset($_GET["id"])) {
?>
	<h1>Edit Character</h1>

	<p>Choose the character to edit:
<?php
}
else
	echo "<p>Edit a different character (any unsaved changes will be lost):\n";
?>
	</p>
	<form method="get">
	<select name="id">
<?php
	selectCharacters($db, 0, 0);
?>
	</select>
	<input type="submit" name="btnChoose" value="Edit">
	</form>

<?php
require("inc_foot.php");
?>
