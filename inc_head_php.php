<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require ("inc_config.php");

try {
	$db = new SQLite3(DBFILE, SQLITE3_OPEN_READWRITE);
	
	// Create tables added since v1.0 if they don't exist
	if ($db->querySingle("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='groups'") == 0)
		$db->exec ("CREATE TABLE 'groups' ('groupid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'groupname' TEXT)");
	if ($db->querySingle("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='groupmembers'") == 0)
		$db->exec ("CREATE TABLE 'groupmembers' ('gm_groupid' INTEGER, 'gm_charid' INTEGER)");
	if ($db->querySingle("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='logins'") == 0)
		$db->exec ("CREATE TABLE 'logins' ('l_playerid' INTEGER, 'token' TEXT, 'expire' INTEGER)");
	if ($db->querySingle("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='availability'") == 0)
		$db->exec ("CREATE TABLE 'availability' ('avid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'playerid' INTEGER, 'date' DATETIME, 'colour' TEXT)");
	if ($db->querySingle("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='config'") == 0) {
		$db->exec ("CREATE TABLE 'config' ('confid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'key', 'val')");
		$sql = "INSERT INTO config (key, val) VALUES ('av_date', '')";
		$db->exec ($sql);
	}

	// Add columns added since v1.0 if they don't exist
	if ($db->querySingle("SELECT gmnotes FROM characters") === False)
		$db->exec ("ALTER TABLE characters ADD COLUMN gmnotes TEXT");
	// Drop/recreate tables that have changed
	if ($db->querySingle("SELECT playerid FROM availability") === False) {
		$db->exec ("DROP TABLE availability");
		$db->exec ("CREATE TABLE 'availability' ('avid' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'playerid' INTEGER, 'date' DATETIME, 'colour' TEXT)");
	}
	
	// Check for newly-defined constants. Set to a sensible value if not set
	if (defined("SITENAME") === False)
		define ("SITENAME", "QAGS Web");
}
catch (Exception $e) {
	// Unable to open database. Redirect to installer
	$url = "http";
	if ($_SERVER["HTTPS"] == "on" || $_SERVER["REQUEST_SCHEME"] == https)
		$url .= "s";
	$url .= "://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/install.php";
	header ("Location:$url");
}
$log = '';

require ('inc_auth.php');
require ('inc_chat.php');

// If user is a guest, unset any POST variables to stop them doing any damage, except on login page
if (ROLE == "guest" && basename($_SERVER['PHP_SELF']) != "login.php")
	unset($_POST);
// Redirect if user does not have rights to view this page
if (substr(basename($_SERVER['PHP_SELF']), 0, 3) == "gm_" && ROLE != "gm") {
	// PHP redirection
	header ("Location:".BASEURL."?msg=notloggedin");
	// JavaScript redirection
	echo "\n<script>\nlocation.href='".BASEURL."'\n</script>\n";
	// echo link as backup
	echo "\n<p>You do not have permission to access this page. <a href='".BASEURL."'>Go to the home page instead.</a></p>\n";
	// Die, so that nothing else is output
	die();
}

// Links. GM only have "gm_" prefix
$links = array (
	"index.php" => "Characters",
	"rolls.php" => "Make a Roll",
	"combat.php" => "Combat",
	"useyy.php" => "Use Yum Yums",
	"dicelog.php" => "Dice Log",
	"diceroller.php" => "Generic Dice Roller",
	"availability.php" => "Availability",
	"gm_addchar.php" => "Add PC/GMC",
	"gm_massedit.php" => "Mass Update PCs/GMCs",
	"gm_editchar.php" => "Edit PC/GMC",
	"gm_groups.php" => "Manage Groups",
	"gm_editplayer.php" => "Add/Edit Player",
);
if (ROLE == "guest")
	$links [LOGINURL] = "Log In";
else {
	$links ["profile.php"] = "User Profile";
	$links [LOGOUTURL] = "Log Out";
}

/*
Log to database and send to XMPP chatroom or IRC channel
*/
function logdb ($str) {
	global $db;
	
	// Insert log entry into database
	$sql = "INSERT INTO log('datetime', 'log') VALUES ('".date ("Y-m-d H:i:s")."', '".$db->escapeString($str)."')";
	$db->exec($sql);

	// Send to IRC channel
	if (USE_IRC === True)
		sendIrcMessage ($str);
	// Send to XMPP chatroom
	if (USE_XMPP === True)
		sendXmppMessage ($str);
}

/*
Output an <option> list of characters, with character ID $selected selected
$activeonly is 1 if only active characters/GMCs are to be included
Return the ID of the selected character
*/
function selectCharacters($db, $selected = 0, $activeonly = 1) {
	$selectedid = 0;
	if ($activeonly == 1)
		$active = " active = 1 AND";
	else
		$active = "";
	$sql = "SELECT characters.*, players.name AS plname
		FROM characters
		LEFT JOIN players ON player_charid = charid
		WHERE$active gmc = 0
		ORDER BY name";
	$pcs = $db->query($sql);
	echo "<optgroup label='Player Characters'>\n";
	while ($pc = $pcs->fetchArray(SQLITE3_ASSOC)) {
		// By default, $selectedid is first character listed
		if ($selectedid == 0)
			$selectedid = $pc["charid"];
		echo "<option value='".$pc["charid"]."'";
		if ($pc["charid"] == intval($selected)) {
			$selectedid = $pc["charid"];
			echo " selected";
		}
		echo ">".htmlentities($pc["name"], ENT_QUOTES);
		if ($pc["plname"] != "")
			echo " (".htmlentities($pc["plname"], ENT_QUOTES).")";
		echo "</option>\n";
	}
	echo "</optgroup>\n";
	
	echo "<optgroup label='GMCs (No Group)'>\n";
		$sql = 'SELECT * FROM characters
			LEFT JOIN groupmembers on charid = gm_charid
			WHERE gm_charid IS NULL
			AND' . $active . '
			gmc = 1
			ORDER BY name';

		$gmcs = $db->query($sql);
		while ($gmc = $gmcs->fetchArray(SQLITE3_ASSOC)) {
			echo "<option value='".$gmc["charid"]."'";
			if ($gmc["charid"] == intval($selected)) {
				$selectedid = $gmc["charid"];
				echo " selected";
			}
			echo ">".htmlentities($gmc["name"], ENT_QUOTES)."</option>\n";
		}
		
		echo '</optgroup>';

	$sql = 'SELECT * FROM groups ORDER BY groupname';
	$groups = $db->query ($sql);
	while ($group = $groups->fetchArray(SQLITE3_ASSOC)) {
		echo '<optgroup label="GMCs (' . htmlentities($group['groupname'], ENT_QUOTES) . ')">';
		
		$sql = 'SELECT * FROM characters
			LEFT JOIN groupmembers on charid = gm_charid
			WHERE gm_groupid = ' . $group['groupid'] . '
			AND' . $active . '
			gmc = 1
			ORDER BY name';

		$gmcs = $db->query($sql);
		while ($gmc = $gmcs->fetchArray(SQLITE3_ASSOC)) {
			echo "<option value='".$gmc["charid"]."'";
			if ($gmc["charid"] == intval($selected)) {
				$selectedid = $gmc["charid"];
				echo " selected";
			}
			echo ">".htmlentities($gmc["name"], ENT_QUOTES)."</option>\n";
		}
		
		echo '</optgroup>';
	}

	echo "</optgroup>\n";
	
	return $selectedid;
}
