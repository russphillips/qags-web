# QAGS Web

This is a web application, intended to be used by people playing the _[QAGS](http://www.hexgames.com/qags2e/)_ RPG online. It can send updates (dice roll results etc) to an [XMPP/Jabber](https://xmpp.org/) chatroom or IRC channel.

A demo install is available at https://rpg.phillipsuk.org/qagswebdemo/

## Requirements

PHP >= 7.0 with SQLite support.

The install directory must be writable by the web server, so that changes can be written to the database.

For XMPP support, [sendxmpp](http://sendxmpp.hostname.sk/) must be installed.

## Installation

* Download the latest version from the [tags page](https://gitlab.com/robinphillips/qags-web/tags)
* Rename/copy `inc_config_sample.php` to `inc_config.php`
* Edit `inc_config.php` to set required values
* Place all files in a web-accessible directory
* Make sure that the web server can write to the database file and directory
* Navigate to the `install.php` file in a web browser
* Fill in the GM details
* Click the Install button

This will create the required SQLite database in the install directory, and set up a GM user. Delete the `install.php` file once installation is complete.

Once logged in as the GM user, you can add players using the "Add/Edit Player" link.

## Upgrades

To upgrade to a newer version, simply upload the files over the old ones.

## Copyright etc

QAGS Web is copyright (c) Robin Phillips. QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor.

You may freely use this software, or give it away to others. You may not charge for copies without the express permission of Hex Games and Robin Phillips. You may alter the code in any way you wish, and give away the changes. Bug reports, patches, etc are welcome. Submit them at the [GitLab project](https://gitlab.com/robinphillips/qags-web).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
