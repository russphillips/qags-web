<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require_once("inc_head_php.php");

// Return a character's HP (current/max)
$ajaxdb = new SQLite3(DBFILE);
$charid = intval($_GET["charid"]);

// Output an <OPTION> tag, selected if it is the default
function echo_option($value, $display) {
	echo "<option";
	if (isset($_GET["default"]) && $_GET["default"] == $value)
		echo " selected";
	echo " value='".htmlentities($value, ENT_QUOTES)."'";
	echo ">".htmlentities($display, ENT_QUOTES)."</option>";
}

$sql = "SELECT currenthp, hp FROM characters WHERE charid = $charid";
$char = $db->querySingle($sql, True);
echo $char["currenthp"]."/".$char["hp"];

$ajaxdb->close();
?>
