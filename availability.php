<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

$title = "Availability";
require("inc_head_php.php");
if (isset($_GET["action"]) && $_GET["action"] == "cleartable" && ROLE == "gm") {
	$sql = "DELETE FROM availability";
	$db->exec($sql);
	header ("Location:" . BASEURL . "availability.php");
}

// Redirect guests to index page
if (ROLE == "guest")
	header("Location:".BASEURL."?msg=guest");

require("inc_head_html.php");

if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	$sql = "UPDATE config SET val = '" . $db->escapeString($_POST["startdate"]) . "' WHERE key = 'av_date'";
	$db->exec($sql);
}

// Start date
$sql = "SELECT val FROM config WHERE key LIKE 'av_date'";
$startdate = $db->querySingle($sql);
// Use default (next Monday) if specific date is not set, or if it is in the past
if (strtotime ("00:01 $startdate") <= time())
	$startts = strtotime("12:00 Next Monday");
else
	$startts = strtotime("12:00 $startdate");
?>
<style>
td {
	background-color: white;
}
</style>

<script>
$(function() {
	$(".clickable").click(function() {
		// Rotate through colours
		if ($(this).css ("background-color") == "rgb(255, 255, 255)")
			state = "green"
		else if ($(this).css ("background-color") == "rgb(0, 128, 0)")
			state = "orange"
		else if ($(this).css ("background-color") == "rgb(255, 165, 0)")
			state = "red"
		else if ($(this).css ("background-color") == "rgb(255, 0, 0)")
			state = "white"

		// Set the new colour
		$(this).css ("background-color", state)
		// Update the title
		if (state == "green")
			$(this).attr("title", "Available")
		if (state == "orange")
			$(this).attr("title", "Not ideal")
		if (state == "red")
			$(this).attr("title", "Not available")
		if (state == "white")
			$(this).attr("title", "Unknown")
		// Save the new colour
		$.get ("availability_sql.php", { playerid: $(this).data('id'), date:$(this).data('date'), colour: state })
	})
})
</script>

<h1>Availability</h1>

<?php
// Set up players array
$players = array();
$sql = "SELECT playerid, name FROM players ORDER BY name";
$resplayers = $db->query($sql);
while ($pname = $resplayers->fetchArray())
	$players[$pname['playerid']] = $pname['name'];

$colours = array (
	'white' => 'Unknown',
	'green' => 'Available',
	'orange' => 'Not ideal',
	'red' => 'Not available',
);

$sql = 'SELECT COUNT(*) FROM players';
$charNum = $db->querySingle($sql);

for ($table = 0; $table <= AVAILABILITY_WEEKS - 1; $table++) {
	// Start of this week is $table*7 days later than $startts
	$dayslater = $table * (7*24*60*60);
	$dayts = $startts + $dayslater;

	echo '<p>Week beginning ' . date('l jS F', $dayts) . '</p>';

	echo '<p><table style="display:inline-block;">';
	echo '<tr><th></th>';
	for ($i=1; $i<=7; $i++) {
		$date = date("Y-m-d", $dayts);
		// Colour the header
		$thcol = 'black';
		$sqlred = "SELECT COUNT(*) FROM availability WHERE date LIKE '$date' AND colour LIKE 'red'";
		$sqlorange = "SELECT COUNT(*) FROM availability WHERE date LIKE '$date' AND colour LIKE 'orange'";
		$sqlgreen = "SELECT COUNT(*) FROM availability WHERE date LIKE '$date' AND colour LIKE 'green'";
		if ($db->querySingle($sqlred) >= 1)
			$thcol = 'red';
		elseif ($db->querySingle($sqlorange) >= 1)
			$thcol = 'chocolate';
		elseif ($db->querySingle($sqlgreen) == $charNum)
			$thcol = 'green';
		echo '<th style="color:' . $thcol . '" id="th' . $date . '">' . date("D jS", $dayts) . '</th>';
		$dayts += 86400;
	}

	$days = array ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
	foreach ($players as $pid => $pn) {
		// Reset the week start date
		$dayts = $startts + $dayslater;

		if ($pn == PLAYERNAME || ROLE == "gm")
			$class = " class='clickable'";
		else
			$class = "";
		echo "<tr><th>$pn</th>";
		for ($i=1; $i<=7; $i++) {
			$date = date("Y-m-d", $dayts);
			$dayts += 86400;
			$sql = "SELECT * FROM availability WHERE playerid = $pid AND date LIKE '$date'";
			$rows = $db->query($sql);
			$row = $rows->fetchArray();
			if ($row === False)
				echo "<td id='$pid"."$date' data-id='$pid' data-date='$date' style='background-color:white;'$class title='Unknown'>&nbsp;</td>";
			else
				echo "<td id='".$row['playerid'].$row['date']."' data-id='".$row['playerid']."' data-date='".$row['date']."' style='background-color:".$row['colour'].";'$class title='".$colours[$row['colour']]."'>&nbsp;</td>";
		}
		echo "</tr>\n";
	}
	echo '</table>&nbsp;&nbsp;&nbsp;';
}
?>

<p>
White: Unknown<br>
Green: Available<br>
Orange: Available, but not ideal<br>
Red: Not available
</p>

<p>
<a href="availability.php">Refresh the table</a>
</p>
<?php
echo "<p>\n";
if (ROLE == "gm") {
	echo "<form method='post'>\n";
	echo "<a href='?action=cleartable'>Clear the table</a></p>\n<p>\n";
	echo "Start date defaults to next Monday. Over-ride start date: <input type='date' name='startdate' placeholder='YYYY-MM-DD' style='width:10em;'>\n";
	echo "<input type='submit' value='Submit' name='btnSubmit'>\n</form>\n";
	echo "</p>\n";
}

// Delete any records dated at least 10 days ago
$date = date('Y-m-d', strtotime("-10 days"));
$sql = "DELETE FROM availability WHERE date < '$date'";
$db->exec($sql);
$sql = "VACUUM availability";
$db->exec($sql);
require("inc_foot.php");
?>
