<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
require("inc_head_html.php");
?>

<h1>Mass Update PCs/GMCs</h1>

<p>
Filter by group:&nbsp;
<select id="group">
<option value="0">Show all</option>
<?php
echo "<option value='-1'";
if (isset($_GET["groupid"]) && intval($_GET["groupid"]) == -1)
	echo " selected";
echo ">Not in a group</option>";

$sql = "SELECT * FROM groups ORDER BY groupname";
$groups = $db->query($sql);
while ($group = $groups->fetchArray(SQLITE3_ASSOC)) {
	echo "<option value='".$group["groupid"]."'";
	if (isset($_GET["groupid"]) && intval($_GET["groupid"]) == $group["groupid"])
		echo " selected";
	echo ">".htmlentities($group["groupname"], ENT_QUOTES)."</option>";
}

echo "</select>\n";

echo "<input type='checkbox' id='chkPC'";
if (!isset($_GET["pc"]) || $_GET["pc"] == "true")
	echo " checked";
echo "><label for='chkPC'> Show PCs</label>\n";

echo "<input type='checkbox' id='chkGMC'";
if (!isset($_GET["gmc"]) || $_GET["gmc"] == "true")
	echo " checked";
echo "><label for='chkGMC'> Show GMCs</label>\n";

echo '<input type="checkbox" id="chkInactive"';
if (isset($_GET['inactive']) && $_GET['inactive'] == 'true')
	echo ' checked';
echo '><label for="chkInactive"> Show inactive PCs &amp; GMCs</label>';
?>
</p>

<?php
if (isset($_POST["btnSubmit"]) && $_POST["btnSubmit"] != "") {
	// Loop through characters in database, update each one
	$sql = "SELECT * FROM characters";
	$characters = $db->query($sql);
	while ($character = $characters->fetchArray()) {
		$charid = $character["charid"];
		// Only act on characters that were displayed - they had the hidden input h_charid set
		if (isset($_POST["h_charid".$charid]) && $_POST["h_charid".$charid] == $charid) {
			// Update "active" column
			if (isset($_POST["active".$charid]))
				$active = 1;
			else
				$active = 0;
			// Clear notes
			if (isset($_POST["clearnote".$charid]))
				$notes = "";
			else
				$notes = $character["notes"];
			// Restore HP
			if (isset($_POST["restorehp".$charid]))
				$currenthp = intval($character["hp"]);
			else {
				// Add/remove HP
				$currenthp = $character["currenthp"] + intval($_POST["adjusthp".$charid]);
			}
			// Add/remove Yum Yums
				$yumyums = $character["yumyums"] + intval($_POST["yumyums".$charid]);

			// Sanity check
			if ($currenthp > intval($character["hp"]))
				$currenthp = intval($character["hp"]);
			if ($currenthp < 0)
				$currenthp = 0;
			if ($yumyums < 0)
				$yumyums = 0;
			
			$sql = "UPDATE characters SET
				active = $active,
				notes = '".$db->escapeString($notes)."',
				currenthp = $currenthp,
				yumyums = $yumyums
				WHERE charid=".$charid;
			$db->exec($sql);
		
			// Groups
			// Delete from all groups, then add to selected groups
			$sql = "DELETE FROM groupmembers WHERE gm_charid = $charid";
			$db->exec($sql);
			// Loop through groups, add character to selected groups
			$groups->reset();
			while ($group = $groups->fetchArray(SQLITE3_ASSOC)) {
				if (isset($_POST["group_".$charid."_".$group["groupid"]])) {
					$sql = "INSERT INTO groupmembers ('gm_groupid', 'gm_charid')
						VALUES (".$group["groupid"].", $charid)";
					$db->exec($sql);
				}
			}

			// Delete
			if (isset($_POST["delete".$charid])) {
				$sql = "DELETE FROM characters WHERE charid=".$charid;
				$db->exec($sql);
				$sql = "DELETE FROM groupmembers WHERE gm_charid = $charid";
				$db->exec($sql);
				$sql = "DELETE FROM words WHERE charid = $charid";
				$db->exec($sql);
				$sql = "UPDATE players SET player_charid = 0 WHERE player_charid = $charid";
				$db->exec($sql);
			}
		}
	}
	
	echo "<p class='good'>Characters updated.</p>";
}

function displayCharacters($db, $gmc) {
	echo "<div id='filter$gmc'>";
	if ($gmc == 0)
		echo "<h2>Player Characters</h2>\n";
	else
		echo "<h2>GMCs</h2>";
	echo "<p>\n";
	echo "Toggle all:
	<span class='donotwrap'><input type='checkbox' id='allactive$gmc' title='check/uncheck all'><label for='allactive$gmc'> Active</label></span>
	<span class='donotwrap'><input type='checkbox' id='allrestore$gmc' title='check/uncheck all'><label for='allrestore$gmc'> Restore HP</label></span>
	<span class='donotwrap'><input type='checkbox' id='allclear$gmc' title='check/uncheck all'><label for='allclear$gmc'> Clear notes</label></span>
	<span class='donotwrap'><input type='checkbox' id='incrementyy$gmc' title='check/uncheck all'><label for='incrementyy$gmc'> +1 Yum Yum</label></span>
	</p>\n";
	if ($gmc == 0)
		$pcgmc = "PC";
	else
		$pcgmc = "GMC";
	
	// Get characters in a group
	if (isset($_GET["groupid"]) && intval($_GET["groupid"]) > 0)
		$sql = "SELECT * FROM characters, groupmembers
			WHERE gmc = $gmc
			AND characters.charid = groupmembers.gm_charid
			AND groupmembers.gm_groupid = ".intval($_GET["groupid"]);
	// Get characters in no groups
	elseif (isset($_GET["groupid"]) && intval($_GET["groupid"]) == -1)
		$sql = "SELECT * FROM characters
			WHERE gmc = $gmc
			AND charid NOT IN
			(SELECT gm_charid FROM groupmembers)";
	// Get all characters
	else
		$sql = "SELECT * FROM characters WHERE gmc = $gmc";
	$sql .= " ORDER BY name";
	$pcs = $db->query($sql);

	while ($pc = $pcs->fetchArray(SQLITE3_ASSOC)) {
		echo '<div class="box character';
		if ($pc['active'])
			echo ' activechar';
		else
			echo ' inactivechar';
		echo '">';
		echo "<p class='boxtitle'>".htmlentities($pc["name"], ENT_QUOTES);
		
		$sql = "SELECT name FROM players WHERE player_charid = ".$pc["charid"];
		$player = $db->querySingle($sql);
		if ($player != "")
			echo "&nbsp;<span class='sml'>(".htmlentities($player, ENT_QUOTES).")</span>";
		echo "<a class='sml' style='float:right;' href='gm_editchar.php?id=".$pc["charid"]."' class='aEdit'>edit</a>";
		echo "</p>\n";
		
		if ($pc["active"] == 1)
			$check = " checked";
		else
			$check = "";
		echo "<div class='innerhalf'>
		<input class='activetick$gmc' type='checkbox'$check name='active".$pc["charid"]."' id='active".$pc["charid"]."'>
		<label for='active".$pc["charid"]."'> Active</label><br>
		<input class='restoretick$gmc' type='checkbox' name='restorehp".$pc["charid"]."' id='restorehp".$pc["charid"]."'>
		<label for='restorehp".$pc["charid"]."'> Restore HP</label>
		</div>
		
		<div class='innerhalf'>
		<input class='cleartick$gmc' type='checkbox' name='clearnote".$pc["charid"]."' id='clearnote".$pc["charid"]."'>
		<label for='clearnote".$pc["charid"]."'> Clear notes</label><br>
		<input class='deletetick$gmc' type='checkbox' name='delete".$pc["charid"]."' id='delete".$pc["charid"]."'>
		<label for='delete".$pc["charid"]."' style='color:red'> Delete $pcgmc</label>
		</div>\n";
		
		echo "<p>Add/remove Yum Yums: <input name='yumyums".$pc["charid"]."' class='small incrementyy$gmc' type='number'><br>";
		echo "Add/remove HP: <input name='adjusthp".$pc["charid"]."' class='small' type='number'></p>";
		
		echo "<p>Current HP: ".intval($pc["currenthp"])."/".intval($pc["hp"])."<br>";
		echo "Current Yum Yums: ".intval($pc["yumyums"])."</p>";
		
		echo "<p><input type='hidden' name='h_charid".$pc["charid"]."' value='".$pc["charid"]."'>";
		$sql = "SELECT * FROM groups ORDER by groupname";
		$groups = $db->query($sql);
		while ($group = $groups->fetchArray(SQLITE3_ASSOC)) {
			// Tick the box if the character is in the group
			$sql = "SELECT COUNT(*) FROM groupmembers WHERE gm_groupid=".$group["groupid"]." AND gm_charid=".$pc["charid"];
			if ($db->querySingle($sql) == 1)
				$check = " checked";
			else
				$check = "";
			echo "<input type='checkbox'$check name='group_".$pc["charid"]."_".$group["groupid"]."' id='g_".$pc["charid"]."_".$group["groupid"]."'><label for='g_".$pc["charid"]."_".$group["groupid"]."'> ".htmlentities($group["groupname"], ENT_QUOTES)."</label>&nbsp;";
		}
		echo "</p>";
		echo "</div>";
	}
	echo "</div>";
}
?>
<script>
// Change background colour if PC/GMC is to be deleted
function deletebg (chk) {
	if ($(chk).prop('checked'))
		$(chk).parent().parent().css("background", "coral")
	else
		$(chk).parent().parent().css("background", "")
}

$(function() {
	// Change handlers for select/deselect all
	$("#allactive0").change(function () {
		$(".activetick0").prop('checked', $(this).prop("checked"))
	})
	$("#allactive1").change(function () {
		$(".activetick1").prop('checked', $(this).prop("checked"))
	})
	$("#allrestore0").change(function () {
		$(".restoretick0").prop('checked', $(this).prop("checked"))
	})
	$("#allrestore1").change(function () {
		$(".restoretick1").prop('checked', $(this).prop("checked"))
	})
	$("#allclear0").change(function () {
		$(".cleartick0").prop('checked', $(this).prop("checked"))
	})
	$("#allclear1").change(function () {
		$(".cleartick1").prop('checked', $(this).prop("checked"))
	})
	$("#alldelete0").change(function () {
		$(".deletetick0").prop('checked', $(this).prop("checked"))
	})
	$("#alldelete1").change(function () {
		$(".deletetick1").prop('checked', $(this).prop("checked"))
	})

	// Change handlers for +1 YY
	$("#incrementyy0").change(function () {
		$(".incrementyy0").val(1)
	})
	$("#incrementyy1").change(function () {
		$(".incrementyy1").val(1)
	})
	
	// Change background colour if PC/GMC is to be deleted
	$(".deletetick0").change(function (event) {
		deletebg ($(this))
	})
	$(".deletetick1").change(function (event) {
		deletebg ($(this))
	})
	
	// Reload if group filter is changed
	$("#group").change (function() {
		url = "<?=BASEURL;?>" + "gm_massedit.php?groupid=" + $(this).val()
		url += "&pc=" + $("#chkPC").prop("checked")
		url += "&gmc=" + $("#chkGMC").prop("checked")
		url += '&inactive=' + $('#chkInactive').prop('checked')
		location.href = url
	})
	
	// Show/hide PCs/GMCs
	function showHidePcGmc () {
		if ($("#chkPC").prop("checked"))
			$("#filter0").show()
		else
			$("#filter0").hide()
			
		if ($("#chkGMC").prop("checked"))
			$("#filter1").show()
		else
			$("#filter1").hide()

		if ($("#chkInactive").prop("checked"))
			$(".inactivechar").show()
		else
			$(".inactivechar").hide()
	}
	$("#chkPC").change(function(event) {
		showHidePcGmc ()
	})
	$("#chkGMC").change(function(event) {
		showHidePcGmc ()
	})
	$("#chkInactive").change(function(event) {
		showHidePcGmc ()
	})
	// Show/hide at startup
	showHidePcGmc ()
})
</script>

<form method="post">
<p><button style='float:right;' onclick='location.href = location.href;'>Cancel</button><input style='clear:both;' type='submit' value='Save changes' name='btnSubmit'></p>
<?php
displayCharacters($db, 0);
displayCharacters($db, 1);
?>
<p><button style='float:right;' onclick='location.href = location.href;'>Cancel</button><input style='clear:both;' type='submit' value='Save changes' name='btnSubmit'></p>
</form>

<?php
require("inc_foot.php");
?>
