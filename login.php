<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require("inc_head_php.php");
require("inc_head_html.php");

$err = "";
$msg = "";
$useremail = "";
$baselink = BASEURL . "login.php";

if (isset($_GET["reset"])) {
	// Get email associated with the reset code
	$sql = "SELECT email FROM players WHERE reset = '".$db->escapeString($_GET["reset"])."'";
	$useremail = $db->querySingle($sql);
}

// Handle logout
if (isset($_GET["action"]) && $_GET["action"] == "logout") {
	// Remove login token from database
	$sql = "DELETE FROM logins WHERE token = '".$db->escapeString($_COOKIE["qw_token"])."'";
	$db->exec($sql);
	// Blank cookies and set expire time to an hour ago so that they are deleted
	foreach ($_COOKIE as $key => $val)
		if (substr($key, 0, 3) == "qw_")
			setcookie ($key, "", time() - 3600);
	// Go to index page
	header("Location:".BASEURL);
}

if (isset($_POST["btnLogin"]) && $_POST["btnLogin"] != "") {
	if (isset($_POST["reset"]) && $_POST["reset"] != "") {
		// User set new password
		$sql = "UPDATE players
			SET password='".$db->escapeString(password_hash ($_POST["password"], PASSWORD_DEFAULT))."', reset = NULL
			WHERE email LIKE '".$db->escapeString($_POST["email"])."'
			AND reset = '".$db->escapeString($_POST["reset"])."'";
		$db->exec($sql);
		// Reload page without reset code
		header("Location:$baselink");
	}
	else {
		// Normal login. Check password
		$sql = "SELECT playerid, password FROM players WHERE email LIKE '".$db->escapeString($_POST["email"])."'";
		$p = $db->querySingle($sql, True);
		$hash = $p ["password"];
		$pid = $p ["playerid"];

		if (password_verify($_POST["password"], $hash)) {
			// Set a token and place it in the logins table and a cookie
			$token = bin2hex(openssl_random_pseudo_bytes(32));
			if (isset($_POST["stayloggedin"]) && $_POST["stayloggedin"] != "") {
				// Set expire time to 30 days hence
				$expire = time()+(60*60*24*30);
				// Set cookies
				setcookie ("qw_stay", 1, $expire);
				setcookie ("qw_token", $token, $expire);
				// Set up SQL to update logins table
				$sql = "INSERT INTO logins ('l_playerid', 'token', 'expire') VALUES ($pid, '$token', $expire)";
			}
			else {
				// Set expire time to 24 hours hence
				$expire = time()+(60*60*24*1);
				// Set cookies
				setcookie ("qw_stay", 0);
				setcookie ("qw_token", $token);
				// Set up SQL to update logins table
				$sql = "INSERT INTO logins ('l_playerid', 'token', 'expire') VALUES ($pid, '$token', $expire)";
			}
			// Add login to logins table
			$db->exec ($sql);
			// Go to index page
			header("Location:".BASEURL);
		}
		else {
			// Login failed
			$err = "Login failed. Please check your email and password.";
		}
	}
}

if (isset($_POST["btnReset"]) && $_POST["f_email"] != "") {
	$err = "";
	// Generate random reset code
	$reset = sha1(openssl_random_pseudo_bytes(64));
	$link = $baselink . "?reset=$reset";
	// Update players table with random reset code. Set password to an invalid value to prevent logins
	$sql = "UPDATE players 
		SET password='RESET', reset='".$db->escapeString($reset)."'
		WHERE email LIKE '".$db->escapeString($_POST["f_email"])."'";
	if ($db->exec($sql) === False)
		$err = "There was a problem resetting your password. Please check you have entered your email correctly.";
	
	// Remove any existing logins
	$sql = "SELECT playerid FROM players WHERE email LIKE '".$db->escapeString($_POST["f_email"])."'";
	$playerid = $db->querySingle($sql);
	$sql = "DELETE FROM logins WHERE l_playerid = $playerid";
	if ($db->exec($sql) === False)
		$err = "There was a problem resetting your password. Please check you have entered your email correctly.";
		
	if ($err == "") {
		// Send link to player
		$msg = "You requested a QAGS Web password reset. Click this link to reset your password:\n$link\n\n";
		$msg .= "You will not be able to log in until your password has been reset.\n\n";
		$msg .= "--\nQAGS Web\n$baselink\n";
		mail ($_POST["f_email"], "QAGS Web password reset", $msg);
		$msg = "A reset link has been emailed to you.";
	}
}

if ($useremail == "")
	echo "<h1>Log In</h1>\n";
else {
	echo "<h1>Reset Password</h1>\n";
	$msg = "Enter your new password below.";
}

if ($err != "")
	echo "<p class='bad'>$err</p>";
if ($msg != "")
	echo "<p class='good'>$msg</p>";
?>

<form method="post">
<p>
<div class="box">
<p>
Email: 
<?php
if ($useremail == "")
	echo "<input name='email'><br>\n";
else {
	echo "<input type='hidden' name='reset' value='".htmlentities($_GET["reset"], ENT_QUOTES)."'>";
	echo "<input type='hidden' name='email' value='".htmlentities($useremail, ENT_QUOTES)."'>";
	echo "$useremail<br>\n";
}
?>
Password: <input type="password" name="password">
</p>
<p>
<?php
if ($useremail == "") {
	// Log in buttons
	echo "<input type='checkbox' name='stayloggedin' id='stayloggedin'> <label for='stayloggedin'>Stay logged in</label>";
	echo "</p><p>";
	echo "<input type='submit' value='Log in' name='btnLogin'>\n";
}
else {
	// Reset password button
	echo "<input type='submit' value='Set new password' name='btnLogin'>\n";
}
?>
</p>
</div>
</p>
<?php
if ($useremail == "") {
?>
	<p>
	<div class="box">
	<p class = "boxtitle">Forgotten password</p>
	<p>
	If you have forgotten your password, simply enter your email below and click the button.<br>
	A link will be emailed to you. Click on that link to set a new password.
	</p>
	<p>
	<input name="f_email">
	</p>
	</p>
	<input type="submit" value="Reset password" name="btnReset">
	</p>
	</div>
	</p>
<?php
}
?>
</form>

<?php
require("inc_foot.php");
?>
