<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/

require ("inc_config.php");

function prepareMessage ($msg) {
	// Remove paragraph tags from log string
	$msg = str_ireplace(array("<p>","</p>"), "", $msg);
	// Turn <BR> tags into newlines
	$msg = str_ireplace(array('<br>', '<br />'), "\n", $msg);
	// Replace double newlines with single ones
	do
		$msg = str_ireplace("\n\n", "\n", $msg, $c);
	while ($c > 0);
	// Remove any remaining HTML tags and convert entities into characters
	$msg = html_entity_decode(strip_tags($msg), ENT_QUOTES);
	// Return prepared message
	return $msg;
}

function sendIrcMessage ($msg) {
	// Prepare the message
	$msg = prepareMessage($msg);

	// IRC can't handle newlines, so $irc_msgs is an array of lines to be sent to IRC
	// Convert newlines into elements of an array
	$ma = explode ("\n", $msg);
	// Make sure that no line is more than 200 characters long
	$irc_msgs = array ();
	foreach ($ma as $s)
		if (strlen($s) > 200) {
			$ss = str_split ($s, 200);
			foreach ($ss as $sss)
				$irc_msgs[] = $sss;
		}
		else
			$irc_msgs[] = $s;

	$irc_server = array();
	//Open the socket connection to the IRC server
	$irc_server['SOCKET'] = @fsockopen(IRC_HOST, IRC_PORT, $errno, $errstr, 2);
	if($irc_server['SOCKET']) {
		// Login. Note that spaces in IRC_Nick are replaced with underscores
		sendIrcCommand('PASS ' . IRC_PASS . "\n\r", $irc_server);
		sendIrcCommand('NICK ' . str_replace(' ', '_', IRC_NICK) . "\n\r", $irc_server);
		// Must have four parameters
		sendIrcCommand('USER ' . str_replace(' ', '_', IRC_NICK) . ' QAGS WEB IRC' . "\n\r", $irc_server);
		while(!feof($irc_server['SOCKET'])) {
			$irc_server['READ_BUFFER'] = fgets($irc_server['SOCKET'], 1024); //get a line of data from the server

			/*
			IRC Sends a "PING" command to the client which must be anwsered with a "PONG"
			Or the client gets Disconnected
			Now lets check to see if we have joined the server
			422 is the message number of the MOTD for the server (The last thing displayed after a successful connection)
			*/
			if(strpos($irc_server['READ_BUFFER'], "422")) {
				//If we have joined the server
				sendIrcCommand('JOIN ' . IRC_CHAN . "\n\r", $irc_server); //Join the chanel
			}
			if(substr($irc_server['READ_BUFFER'], 0, 6) == "PING :") {
				//If the server has sent the ping command
				sendIrcCommand("PONG :".substr($irc_server['READ_BUFFER'], 6)."\n\r", $irc_server); //Reply with pong
			}

			sendIrcCommand('JOIN ' . IRC_CHAN . "\n\r", $irc_server); //Join the chanel
			// Send messages
			foreach ($irc_msgs as $msg)
				sendIrcCommand('PRIVMSG ' . IRC_CHAN . " :$msg\n\r", $irc_server);

			flush(); //This flushes the output buffer
			// Disconnect
			sendIrcCommand ("QUIT\n\r", $irc_server);
		}
	}
}

function sendIrcCommand ($cmd, $irc_server) {
	@fwrite($irc_server['SOCKET'], $cmd, strlen($cmd));
}

function sendXmppMessage ($msg) {
	// Prepare the message
	$msg = prepareMessage($msg);
	// Escape message for use as a shell argument
	$arg = escapeshellarg($msg);
	// Run sendxmpp
	exec ("echo $arg | ".XMPP_EXE." --jserver '".XMPP_SERVER."' --username '".XMPP_USER."' --password '".XMPP_PASSWORD."' --tls --resource '".XMPP_RESOURCE."' --chatroom '".XMPP_CHATROOM."'");
}
