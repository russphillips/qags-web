<?php
/*
QAGS Web. Copyright (c) Robin Phillips
QAGS Second Edition is copyright (c) Steve Johnson and Leighton Connor
*/
?>

<hr>
<p class="footerlinks">
:
<?php
foreach ($links as $url => $label) {
	// Do not display GM-only links
	if (substr($url, 0, 3) != "gm_")
		echo "<a href='$url'>$label</a> : ";
}
// GM-only links
if (ROLE == "gm") {
	echo "</p><p class='footerlinks'>GM-only links: ";
	foreach ($links as $url => $label)
		if (substr($url, 0, 3) == "gm_")
			echo "<a class='gmonlylink' href='$url'>$label</a> : ";
}
echo "</p>\n";

if (count($extlinks) > 0 || (ROLE == "gm" && count($gm_extlinks) > 0)) {
	echo "<p class='footerlinks'>Extra links:\n";
	foreach ($extlinks as $url => $label)
		echo "<a href='$url' target='_blank'>$label</a> : ";
}
if (ROLE == "gm" && count($gm_extlinks) > 0) {
	foreach ($gm_extlinks as $url => $label)
		echo "<a class='gmonlylink' href='$url' target='_blank'>$label</a> : ";
}

if (count($extlinks) > 0 || (ROLE == "gm" && count($gm_extlinks) > 0))
	echo "</p>";

if (ROLE == "guest")
	echo "<p>You are viewing as a guest. Guests have limited access. <a href='".LOGINURL."'>Log in</a></p>";

if (ROLE == "gm" && file_exists("install.php"))
	echo "<p class='bad'>The <tt><a href='install.php'>install.php</a></tt> file exists. It should be deleted.</p>\n";
?>

<p class="footerlinks" style="float:right;">
<a href="about.php">QAGS Web</a> by <a href="https://rpg.phillipsuk.org/" title="Robin's RPG Resources">Robin Phillips</a>, for GMs and players of <a href="http://www.drivethrurpg.com/product/28315/QAGS-Second-Edition?it=1&affiliate_id=235519" target="_blank" title="QAGS on DriveThruRPG (affiliate link)">QAGS Second Edition</a> by <a href="http://www.hexgames.com/" target="_blank" title="Hex Games website">Hex Games</a>.
</p>
</body>
</html>

<?php
$db->close();
?>
